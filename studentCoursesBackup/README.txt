- Notes about the code:
The tree design was adapted from the codes in Comen's Algorithms textbook.

- Compilation:
ant -buildfile src/build.xml all

- Cleaning:
ant -buildfile src/build.xml clean

- Execution:
ant -buildfile src/build.xml run -Darg0=FIRST -Darg1=SECOND -Darg2=THIRD -Darg3=FOURTH -Darg4=FIFTH

- Academic Honesty:
"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.�

July 6th, 2017

- References:
http://algorithms.tutorialhorizon.com/inorder-traversal-non-recursive-approach/