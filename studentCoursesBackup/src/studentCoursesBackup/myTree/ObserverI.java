package studentCoursesBackup.myTree;

/**
 * Created by mido_ on 7/1/2017.
 */
public interface ObserverI {
    public void update(Node.Operations op, String course);
}
