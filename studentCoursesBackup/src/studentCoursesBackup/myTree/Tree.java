package studentCoursesBackup.myTree;

import studentCoursesBackup.util.Results;

import java.util.Stack;

/**
 * Created by mido_ on 7/3/2017.
 */
public class Tree {
    Node root = null;

    public Node getRoot(){
        return root;
    }

    public void insertNode(Node nodeIn){
        //Simple check to avoid insertion of duplicate node
        //if(findNode(root, nodeIn.getbNumber()) != null)
        //    return;

        Node nodeY = null;
        Node nodeX = root;
        while (nodeX != null){
            nodeY = nodeX;
            if(nodeIn.getbNumber() < nodeX.getbNumber()){
                nodeX = nodeX.left;
            } else
                nodeX = nodeX.right;
        }

        nodeIn.parent = nodeY;
        if (nodeY == null){
            root = nodeIn;
        } else if(nodeIn.getbNumber() < nodeY.getbNumber()){
            nodeY.left = nodeIn;
        } else {
            nodeY.right = nodeIn;
        }
    }


    public Node findNode(Node start, int bNumIn){
        //Recursive search function
        if (start == null)
            return null;

        if(start.getbNumber() == bNumIn){
            return start;
        }

        if (bNumIn < start.getbNumber()){
            return findNode(start.left, bNumIn);
        } else{
            return findNode(start.right, bNumIn);
        }
    }

    public void deleteNode(Node nodeIn){
        if(nodeIn.left == null){
            transplant(nodeIn, nodeIn.right);
        } else if(nodeIn.right == null){
            transplant(nodeIn, nodeIn.left);
        } else{
            Node nodeY = findMin(nodeIn.right);
            if(nodeY.parent != nodeIn){
                transplant(nodeY, nodeY.right);
                nodeY.right = nodeIn.right;
                nodeY.right.parent = nodeY;
            }

            transplant(nodeIn, nodeY);
            nodeY.left = nodeIn.left;
            nodeY.left.parent = nodeY;
        }
    }

    private Node findMin(Node start){
        while(start.left != null){
            start = start.left;
        }
        return start;
    }

    private Node findMax(Node start){
        while(start.right != null){
            start = start.right;
        }
        return start;
    }

    private void transplant(Node nodeU, Node nodeV){
        //Replaces subtree rooted at U with subtree rooted at V
        if(nodeU.parent == null)
            root = nodeV;
        else if (nodeU == nodeU.parent.left){
            nodeU.parent.left = nodeV;
        } else {
            nodeU.parent.right = nodeV;
        }

        if (nodeV != null)
            nodeV.parent = nodeU.parent;
    }

    public void printTree(Node start, Results resultsOut){
        //Traverses tree and prints the elements in-order
        //if (start != null){
        //    printTree(start.left, stringIn);
        //    stringIn.concat(start.getbNumber() + ": " + start.printCourses());
        //    //System.out.println(start.getbNumber() + ": " + start.printCourses());
        //    printTree(start.right, stringIn);
        //}

        //Non-recursive tree traversal
        //http://algorithms.tutorialhorizon.com/
        //inorder-traversal-non-recursive-approach/
        Stack<Node> s = new Stack<Node>();
        while (true) {
            // Go to the left extreme insert all the elements to stack
            while (start != null) {
                s.push(start);
                start = start.left;
            }
            // check if Stack is empty, if yes, exit from everywhere
            if (s.isEmpty()) {
                return;
            }
            // pop the element from the stack , print it and add the nodes at
            // the right to the Stack
            start =s.pop();
            resultsOut.storeNewResult(start.getbNumber() + ": " + start.printCourses());
            //System.out.println(start.getbNumber() + ": " + start.printCourses());
            start = start.right;
        }
    }
}
