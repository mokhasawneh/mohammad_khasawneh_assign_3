package studentCoursesBackup.myTree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mido_ on 7/1/2017.
 */
public class Node implements Cloneable, ObserverI, SubjectI{
    public Node left;
    public Node parent;
    private int bNumber;
    List<String> courses = new ArrayList<String>();
    public Node right;

    List<Node> listeners = new ArrayList<Node>();

    enum Operations {
        ADD, DELETE;
    }

    public Node(String key){
        setbNumber(key);
        parent = null;
        left = right = null;

    }

    public void setbNumber(String bNumberIn){
        if(bNumberIn.length() != 4){
            System.err.println("Invalid length of B-Number read: " + bNumberIn);
            System.exit(-1);
        } else{
            bNumber = Integer.parseInt(bNumberIn);
        }
    }

    public int getbNumber(){
        return bNumber;
    }

    public void registerCourse(String courseIn){
        String formalCourseName = courseIn.toUpperCase();
        if(!validateCourseName(formalCourseName)){
            System.err.println("Invalid course name found: " + courseIn + " ... ignoring");
            //System.exit(-1);
            return;
        }

        if(!courses.contains(formalCourseName))
            courses.add(formalCourseName);

        notifyObservers(Operations.ADD, courseIn);
    }

    public void removeCourse(String courseIn){
        String formalCourseName = courseIn.toUpperCase();
        if(!validateCourseName(formalCourseName)){
            System.err.println("Invalid course name found: " + courseIn + " ... ignoring");
            //System.exit(-1);
            return;
        }

        courses.remove(courseIn);

        notifyObservers(Operations.DELETE, courseIn);

    }

    private boolean validateCourseName(String courseIn) {
        return courseIn.matches("[A-K]");
    }

    @Override
    public void update(Operations operation, String courseIn) {
        switch (operation){
            case ADD:
                //Extra bit of error catching
                if(!courses.contains(courseIn))
                    courses.add(courseIn);
                break;
            case DELETE:
                if(courses.contains(courseIn))
                    courses.remove(courseIn);
                break;
            default:
                 break;
        }

    }

    @Override
    public void registerObserver(ObserverI o) {
        listeners.add((Node) o);
    }

    @Override
    public void removeObserver(ObserverI o) {
        listeners.remove(o);
    }

    @Override
    public void notifyObservers(Operations operation, String courseIn) {
        for (Node listener:listeners) {
            listener.update(operation, courseIn);
        }

    }

    public String printCourses() {
        String fmtCourses = "";
        for(String x:courses){
            fmtCourses = fmtCourses.concat(x + ", ");
        }

        return fmtCourses;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        //Object clone = super.clone();

        Node clonedNode = new Node(Integer.toString(getbNumber()));
        clonedNode.courses.addAll(courses);

        return clonedNode;
    }
}
