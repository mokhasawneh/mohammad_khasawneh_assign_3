package studentCoursesBackup.myTree;

import java.util.ArrayList;

/**
 * Created by mido_ on 7/1/2017.
 */
public interface SubjectI {
    ArrayList<ObserverI> observerRegistry = new ArrayList<ObserverI>();

    public void registerObserver(ObserverI o);
    public void removeObserver (ObserverI o);
    public void notifyObservers(Node.Operations op, String course); //Notifies all observers
}
