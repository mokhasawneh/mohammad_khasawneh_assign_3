package studentCoursesBackup.driver;

import studentCoursesBackup.myTree.Node;
import studentCoursesBackup.myTree.Tree;
import studentCoursesBackup.util.FileProcessor;
import studentCoursesBackup.util.Results;
import studentCoursesBackup.util.TreeBuilder;

/**
 * Created by mido_ on 7/1/2017.
 */
public class Driver {
    public static void main(String args[]) {
        if (args.length != 5) {
            System.out.println("Usage: <input.txt> <delete.txt> <output1.txt> <output2.txt> <output3.txt>");
            System.exit(-1);
        }

        FileProcessor fileProcessor = new FileProcessor(args[0]);
        Results results = new Results(args[2]);

        TreeBuilder tb = new TreeBuilder();
        Tree mainTree = new Tree();
        Tree bkpTree1 = new Tree();
        Tree bkpTree2 = new Tree();

        String line;

        //Insertion operations
        while ((line = fileProcessor.readLine()) != null) {
            if (!line.isEmpty()) {
                String words[] = line.split(":");

                String bNum = "";
                String courseName = "";
                try {
                    bNum = words[0];
                    courseName = words[1];
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                    System.err.println("Invalid input line format found: " + line);
                    System.exit(-1);
                }

                Node node;
                if ((node = mainTree.findNode(mainTree.getRoot(), Integer.parseInt(bNum))) != null) {
                    //Student already registerd in database
                    //Only add the required course
                    node.registerCourse(courseName);
                    //Listeners will be updated automatically
                } else {
                    //Student not found
                    //Create new node and insert into tree
                    node = tb.createNewNode(bNum, courseName);
                    Node bkpNode1 = null;
                    Node bkpNode2 = null;
                    try {
                        bkpNode1 = (Node) node.clone();
                        bkpNode2 = (Node) node.clone();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                        System.exit(-1);
                    }

                    node.registerObserver(bkpNode1);
                    node.registerObserver(bkpNode2);

                    mainTree.insertNode(node);
                    bkpTree1.insertNode(bkpNode1);
                    bkpTree2.insertNode(bkpNode2);
                }
            }
        }


        //Deletion operations
        fileProcessor = new FileProcessor(args[1]);
        while ((line = fileProcessor.readLine()) != null) {
            if (!line.isEmpty()) {
                String words[] = line.split(":");

                String bNum = "";
                String courseName = "";
                try {
                    bNum = words[0];
                    courseName = words[1];
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                    System.err.println("Invalid input line format found: " + line);
                    System.exit(-1);
                }

                Node node;
                if ((node = mainTree.findNode(mainTree.getRoot(), Integer.parseInt(bNum))) != null) {
                    //Student found in database
                    node.removeCourse(courseName);
                    //Listeners will be updated automatically
                } else {
                    //Student not found
                    //Print warning and ignore
                    System.err.println("Student not in database, ignoring");
                }
            }

            Results mainResults = new Results(args[2]);
            Results bkp1Results = new Results(args[3]);
            Results bkp2Results = new Results(args[4]);

            tb.printTree(mainTree, mainTree.getRoot(), mainResults);
            tb.printTree(mainTree, mainTree.getRoot(), bkp1Results);
            tb.printTree(mainTree, mainTree.getRoot(), bkp2Results);

            mainResults.printAllToFile();
            mainResults.close();

            bkp1Results.printAllToFile();
            bkp1Results.close();

            bkp2Results.printAllToFile();
            bkp2Results.close();
        }
    }
}
