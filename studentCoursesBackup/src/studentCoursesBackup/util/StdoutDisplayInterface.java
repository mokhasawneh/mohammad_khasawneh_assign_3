package studentCoursesBackup.util;

/**
 * Created by mido_ on 6/2/2017.
 */
public interface StdoutDisplayInterface {
    public abstract void writeToStdout(String s) ;
}
