package studentCoursesBackup.util;

import studentCoursesBackup.myTree.Node;
import studentCoursesBackup.myTree.Tree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mido_ on 7/1/2017.
 */
public class TreeBuilder {
    Tree mainTree;

    List<Tree> backupTrees = new ArrayList<Tree>();

    public void insertNode(Tree t, Node nodeIn){
        t.insertNode(nodeIn);
    }

    public Node findNode(Tree t, Node start, int bNumIn){
        return t.findNode(start, bNumIn);
    }

    public void printTree(Tree t, Node start, Results results){
        String tree = "";
        t.printTree(t.getRoot(), results);
    }

    public Node createNewNode(String bNum, String courseName) {
        //Creates and inserts a new node into the main tree
        Node node;
        node = new Node(bNum);
        node.registerCourse(courseName);

        return node;
    }
}
