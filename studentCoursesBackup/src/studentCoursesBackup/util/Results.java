package studentCoursesBackup.util;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by mido_ on 6/2/2017.
 */
public class Results implements  FileDisplayInterface, StdoutDisplayInterface{
    private ArrayList<String> results = new ArrayList<String>();
    private PrintWriter printWriter;

    public Results(String filename){
        try {
            printWriter = new PrintWriter(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            System.exit(-1);
        }
    }

    public void storeNewResult(String s){
        results.add(s);
    }


    @Override
    public void writeToStdout(String s) {
        System.out.println(results);
    }

    @Override
    public void writeToFile(String s) {
        printWriter.print(s);
    }

    public void printAllToFile(){
        for (String s: results) {
            printWriter.println(s);
        }
    }

    public void printAllToScreen(){
        for(String s: results){
            System.out.println(s);
        }
    }

    public void close() {
        printWriter.close();
    }
}
