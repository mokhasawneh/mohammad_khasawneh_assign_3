package studentCoursesBackup.util;

/**
 * Created by mido_ on 6/2/2017.
 */
public interface FileDisplayInterface {
    public abstract void writeToFile(String s) ;
}
