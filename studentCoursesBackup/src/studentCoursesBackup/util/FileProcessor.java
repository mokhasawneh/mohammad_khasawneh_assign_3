package studentCoursesBackup.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by mido_ on 6/2/2017.
 */
public class FileProcessor {
    private BufferedReader bufferedReader;

    /**
     * Handles opening the specified input file
     * @param filename The file to be located and opened
     */
    public FileProcessor(String filename) {
        try {
            bufferedReader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Handles reading and returning lines from file
     * @return The line if not end of file reached
     */
    public String readLine(){
        try {
            return bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return null;
    }
}
